package me.varam.common.exceptions

/**
 * @author : daniil.mironov
 * Created : 11.12.2021
 **/
class IllegalNumberTokenException : RuntimeException()
