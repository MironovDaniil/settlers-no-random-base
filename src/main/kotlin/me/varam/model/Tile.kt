package me.varam.model

import me.varam.common.exceptions.IllegalNumberTokenException

/**
 * @author : daniil.mironov
 * Created : 11.12.2021
 **/
data class Tile (val resourceType: ResourceType, val numberToken: Int) {
    init {
        if (numberToken == 7 || numberToken < 2 || numberToken > 12) {
            throw IllegalNumberTokenException()
        }
    }
}
